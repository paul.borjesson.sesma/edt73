package com.example.edt73;

import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class BodyMass {

    public static void calcAll(RadioGroup rg1, TextInputLayout heightTIL, RadioGroup rg2, RadioGroup rg3, TextInputLayout waistageTIL, TextView txt, TableLayout tl) {
        String tmp = "";
        int height;
        try {
            height = Integer.parseInt(heightTIL.getEditText().getText().toString().trim());
        } catch (NumberFormatException e) {
            e.getStackTrace();
            height = 0;
        }
        int waistAge;
        int option = rg1.getCheckedRadioButtonId();
        switch (option) {
            case R.id.radioButton1:
                waistAge = Integer.parseInt(waistageTIL.getEditText().getText().toString().trim());
                tmp = relativeFatMass(height, getGender(rg2), waistAge) + " %";
                tl.setAlpha(1);
                break;
            case R.id.radioButton2:
                tmp = wanDerVael(height, getGender(rg2)) + " kg";
                break;
            case R.id.radioButton3:
                waistAge = Integer.parseInt(waistageTIL.getEditText().getText().toString().trim());
                tmp = lorentz(height, getGender(rg2), waistAge) + " kg";
                break;
            case R.id.radioButton4:
                waistAge = Integer.parseInt(waistageTIL.getEditText().getText().toString().trim());
                tmp = creff(height, getMorphology(rg3), waistAge) + " kg";
                break;
        }
        txt.setText(tmp);
    }

    public static double relativeFatMass(int height, Gender gender, int waist) {
        int n;
        if (gender.equals(Gender.MALE)) {
            n = Constants.rfn_Male;
        } else {
            n = Constants.rfn_Female;
        }
        return n - (20 * height / waist);
    }

    public static double wanDerVael(int height, Gender gender) {
        float m;
        if (gender.equals(Gender.MALE)) {
            m = Constants.wv_Male;
        } else {
            m = Constants.wv_Female;
        }
        return (height - 150) * m + 50;
    }

    public static double lorentz(int height, Gender gender, int age) {
        float k;
        if (gender.equals(Gender.MALE)) {
            k = Constants.lorentz_Male;
        } else {
            k = Constants.lorentz_Female;
        }
        return height - 100 - (height - 150) / 4 + (age - 20) / 4;
    }

    public static double creff(int height, Morphology morphology, int age) {
        double val = (height - 100 + age / 10);
        if (morphology.equals(Morphology.SMALL)) {
            return val * Math.pow(0.9, 2);
        } else if (morphology.equals(Morphology.MEDIUM)) {
            return val * 0.9;
        } else {
            return val * 0.9 * 1.1;
        }
    }

    public static Gender getGender(RadioGroup rg) {
        int option = rg.getCheckedRadioButtonId();
        switch (option) {
            case R.id.radioButton5:
                return Gender.MALE;
            case R.id.radioButton6:
                return Gender.FEMALE;
        }
        return null;
    }

    public static Morphology getMorphology(RadioGroup rg) {
        int option = rg.getCheckedRadioButtonId();
        switch (option) {
            case R.id.radioButton7:
                return Morphology.SMALL;
            case R.id.radioButton8:
                return Morphology.MEDIUM;
            case R.id.radioButton9:
                return Morphology.BROAD;
        }
        return null;
    }

}
