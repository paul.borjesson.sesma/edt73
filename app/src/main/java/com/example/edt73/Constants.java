package com.example.edt73;

public interface Constants {

    int rfn_Male = 64;
    int rfn_Female = 76;

    float wv_Male = 0.75f;
    float wv_Female = 0.6f;

    float lorentz_Male = 4f;
    float lorentz_Female = 2.5f;

}
