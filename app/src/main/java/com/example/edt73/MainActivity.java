package com.example.edt73;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private RadioGroup radioGroup3;

    private TextInputLayout textInputLayout1;
    private TextInputLayout textInputLayout2;

    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private RadioButton radioButton3;
    private RadioButton radioButton4;
    private RadioButton radioButton5;
    private RadioButton radioButton6;
    private RadioButton radioButton7;
    private RadioButton radioButton8;
    private RadioButton radioButton9;

    private Button button1;

    private TextView txtView1;

    private TableLayout tabLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        //Hooks
        radioGroup1 = findViewById(R.id.radioGroup1);
        radioGroup2 = findViewById(R.id.radioGroup2);
        radioGroup3 = findViewById(R.id.radioGroup3);

        textInputLayout1 = findViewById(R.id.textInputLayout1);
        textInputLayout2 = findViewById(R.id.textInputLayout2);

        radioButton1 = findViewById(R.id.radioButton1);
        radioButton2 = findViewById(R.id.radioButton2);
        radioButton3 = findViewById(R.id.radioButton3);
        radioButton4 = findViewById(R.id.radioButton4);
        radioButton5 = findViewById(R.id.radioButton5);
        radioButton6 = findViewById(R.id.radioButton6);
        radioButton7 = findViewById(R.id.radioButton7);
        radioButton8 = findViewById(R.id.radioButton8);
        radioButton9 = findViewById(R.id.radioButton9);

        button1 = findViewById(R.id.button1);

        txtView1 = findViewById(R.id.txtView1);

        tabLayout1 = findViewById(R.id.tabLayout1);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioSelected = radioGroup1.getCheckedRadioButtonId();
                switch (radioSelected) {
                    case R.id.radioButton1:
                        reset(textInputLayout1, radioGroup2, radioGroup3, textInputLayout2, button1, txtView1, tabLayout1);

                        textInputLayout1.setAlpha(1);
                        radioGroup2.setAlpha(1);
                        radioButton5.setClickable(true);
                        radioButton6.setClickable(true);
                        textInputLayout2.setAlpha(1);
                        textInputLayout2.setHint("Waist circumference (cm)");
                        button1.setAlpha(1);
                        button1.setClickable(true);
                        txtView1.setAlpha(1);
                        break;
                    case R.id.radioButton2:
                        reset(textInputLayout1, radioGroup2, radioGroup3, textInputLayout2, button1, txtView1, tabLayout1);

                        textInputLayout1.setAlpha(1);
                        radioGroup2.setAlpha(1);
                        radioButton5.setClickable(true);
                        radioButton6.setClickable(true);
                        textInputLayout2.setHint("");
                        button1.setAlpha(1);
                        button1.setClickable(true);
                        txtView1.setAlpha(1);
                        break;
                    case R.id.radioButton3:
                        reset(textInputLayout1, radioGroup2, radioGroup3, textInputLayout2, button1, txtView1, tabLayout1);

                        textInputLayout1.setAlpha(1);
                        radioGroup2.setAlpha(1);
                        radioButton5.setClickable(true);
                        radioButton6.setClickable(true);
                        textInputLayout2.setAlpha(1);
                        textInputLayout2.setHint("Age (in years)");
                        button1.setAlpha(1);
                        button1.setClickable(true);
                        txtView1.setAlpha(1);
                        break;
                    case R.id.radioButton4:
                        reset(textInputLayout1, radioGroup2, radioGroup3, textInputLayout2, button1, txtView1, tabLayout1);

                        textInputLayout1.setAlpha(1);
                        radioGroup3.setAlpha(1);
                        radioButton7.setClickable(true);
                        radioButton8.setClickable(true);
                        radioButton9.setClickable(true);
                        textInputLayout2.setAlpha(1);
                        textInputLayout2.setHint("Age (in years)");
                        button1.setAlpha(1);
                        button1.setClickable(true);
                        txtView1.setAlpha(1);
                        break;
                    default:
                        break;
                }

                button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BodyMass.calcAll(radioGroup1, textInputLayout1, radioGroup2, radioGroup3, textInputLayout2, txtView1, tabLayout1);
                    }
                });
            }
        });
    }

    public static void reset(TextInputLayout textInputLayout1, RadioGroup radioGroup2, RadioGroup radioGroup3, TextInputLayout textInputLayout2, Button button1, TextView txtView1, TableLayout tabLayout1) {
        textInputLayout1.setAlpha(0);
        textInputLayout1.getEditText().getText().clear();
        radioGroup2.setAlpha(0);
        radioGroup3.setAlpha(0);
        radioGroup2.clearCheck();
        radioGroup3.clearCheck();
        for (int i = 0; i < 2; i++) {
            radioGroup2.getChildAt(i).setClickable(false);
            radioGroup3.getChildAt(i).setClickable(false);
        }
        radioGroup3.getChildAt(2).setClickable(false);
        textInputLayout2.setAlpha(0);
        textInputLayout2.getEditText().getText().clear();
        button1.setAlpha(0);
        button1.setClickable(false);
        txtView1.setAlpha(0);
        txtView1.setText("");
        tabLayout1.setAlpha(0);

    }
}